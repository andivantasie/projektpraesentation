#!/bin/bash

clear

latexmk -pvc -pdf main.tex

date=$(date '+%Y-%m-%d')
cp main.pdf "builds/${date}-slides-ihk.pdf"

latexmk -C
shopt -s extglob
rm main!(.tex)
